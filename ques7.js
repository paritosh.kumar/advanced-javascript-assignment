class Student {
    constructor(name, id, age){
        this.name = name
        this.id = id
        this.age = age
    }

    generateScore(next) {
       const score = Math.floor(Math.random()*100)
       next(score)
       return score
    }
}

class School {
    constructor(cutOff, students){
        this.cutOff = cutOff
        this.students = students 
    }

    getScoresOfAllStudents() {
        this.students.forEach(student=>{
            const score = student.generateScore(console.log)
            console.log(score>=this.cutOff ? 'Passed' : 'Fail')
        })
        
    }

}

const getRandomInt = (range)=> Math.floor(Math.random()*range)


const Students = []

for (let i = 0; i < 5; i++) {
    Students.push(new Student('name',getRandomInt(20),getRandomInt(5)))
}
const DPS = new School(55,Students)

DPS.getScoresOfAllStudents()