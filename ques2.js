
    // 2nd question

    const search_query = 'node html'
    

    fetch(`https://api.github.com/search/repositories?q=${search_query.split(' ').join('+')}`)
        .then(res=>res.json())
        .then(data=>{
            data.items.forEach(el => {
                // console.log(el)
                let result = {}
                let numberOfBranch = 0
                const {
                    name,
                    full_name,
                    private,
                    owner:
                    {
                        login,
                        url
                    },
                    license:{
                        name:licenseName
                    },
                    score,
                    branches_url
                } = el
                // console.log(branches_url.slice(0,branches_url.indexOf('{')))
                fetch(branches_url.slice(0,branches_url.indexOf('{')))
                    .then(res=>res.json())
                    .then(data=>numberOfBranch = data.length)


                fetch(url)
                    .then(res=>res.json())
                    .then(owner=>{
                        const {
                            name:owenerName,
                            following:followingCount,
                            followers:followersCount
                        } = owner
                        
                        result = {
                                name,
                                full_name,
                                private,
                                owner:
                                {
                                    login,
                                    name:owenerName,
                                    followingCount,
                                    followersCount
                                },
                                licenseName,
                                score,
                                numberOfBranch
                        }
                        console.log(result)
                    })
            });
        })
        .catch(console.error)

// solving question 2 using asyn await 

// async function getRepoInfo(query) {
//     let res = await fetch(`https://api.github.com/search/repositories?q=${query.split(' ').join('+')}`)
//     let result = await res.json()
//     result.items.forEach(el=>{
//         console.log(el)
//         getOwnerInfo(el)
//     })
    
// }

// getRepoInfo(search_query)

// async function getOwnerInfo(el) {
//         let ownerInfoRes = await fetch(el.owner.url)
//         let ownerInfo = await ownerInfoRes.json()

//         let branchInfoRes = await fetch(el.branches_url)
//         let branchInfo = await branchInfoRes.json()

//         console.log(ownerInfo,branchInfo)
// }

