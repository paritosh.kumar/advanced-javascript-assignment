fetch('http://api.nobelprize.org/v1/prize.json')
    .then(res=>res.json())
    .then(data => {
        const yearRangedPizes = data.prizes.filter(el=>el.year>=2000 && el.year<=2019).filter(el=>el.category==="chemistry")
        console.log('prizes in year 2000 to 2019 for chemistry: ')
        console.table(yearRangedPizes)
    })
    .catch(console.error)


// using async await 

async function getResults () {
    const res = await fetch('http://api.nobelprize.org/v1/prize.json')
    const prizes = await res.json()
    const yearRangedPizes = prizes.prizes.filter(prize=>prize.year>=2000 && prize.year<=2019).filter(prize=>prize.category==="chemistry")
    console.log('prizes in year 2000 to 2019: ')
    console.table(yearRangedPizes)
}
