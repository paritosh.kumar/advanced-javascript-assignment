const button = document.querySelector('#questions')

button.addEventListener('click',(e)=>{
    if(e.target.tagName!=='LI')
        return
    const question = e.target.dataset.ques
    let newScript = document.createElement('script')
    newScript.type = "application/javascript"
    newScript.src = `ques${question}.js`
    const scripts = document.querySelectorAll('.question-script')

    
    newScript.classList.add('question-script')

    
    let present = false
    scripts.forEach(el=> {
        if(el.src===newScript.src)
            present = true
    })
    if(!present){
        console.clear()
        document.body.appendChild(newScript)
    }
})