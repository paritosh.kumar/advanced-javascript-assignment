
// helper functions for converting data to objects and arrays

// this adds key-value pairs to object 'obj' and creates a new set for it as the key value
const addToObjectSet = (obj, key, value) => obj.hasOwnProperty(key) ? obj[key].add(value) : obj[key] = new Set([value])

// converts an object to an array of singleObj 
const convertObjectToArray = (obj, arr, singleObj) => {
    for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
            const dataArr = Array.from(obj[key]);
            arr.push(
                singleObj(key,dataArr)
            )
        }
    }
}

// Object definition for genre object
const GenreObject = (type,movies) => ({
    Type: type,
    Movies: movies
})

// Object definition for actor object
const ActorObject = (name, movies) => ({
    Name: name,
    Movies: movies
})

// api call function
async function getDataFromApi(){
    const res = await fetch('https://raw.githubusercontent.com/prust/wikipedia-movie-data/master/movies.json')
    const movies = await res.json()

    // this stores the genres and actors data while iterating over the movies json
    const genres = {}
    const actors = {}

    movies.forEach(movie => {
        movie.genres.forEach( (genre) => addToObjectSet(genres,genre,movie.title) )
        movie.cast.forEach( (actor) => addToObjectSet(actors,actor,movie.title) )
    });

    const Genres = []
    const Actors = []

    convertObjectToArray(genres, Genres, GenreObject)
    convertObjectToArray(actors, Actors, ActorObject)

    console.log(Genres)
    console.log(Actors)
}

getDataFromApi()