
const FILE_URL = 'battles.json'



const GetMaxCountKey = (obj) => {
    let count = 0
    let max_obj_key = ''
    for (const key in obj) {
            count = Math.max(count, obj[key])
            if(count === obj[key]) max_obj_key = key
    }
    return max_obj_key
}

const CreateObjectFromArray = (obj,key,arr) => arr.forEach( (el) =>  el[key] in obj  ? obj[el[key]]++ : obj[el[key]] = 1 )
    

// Question 1

fetch(FILE_URL)
    .then(res => res.json())
    .then((data) => {
        const regions = {}
        const attacker_kings = {}
        const defender_kings = {}
        const attacker_outcomes = {}
        const battle_types = new Set()
        const defender_sizes = []
        data.forEach(battle => {
            battle_types.add(battle['battle_type'])
            if (battle['defender_size'])
            defender_sizes.push(battle['defender_size'])
        });

        CreateObjectFromArray(regions,'region',data)
        CreateObjectFromArray(attacker_kings,'attacker_king',data)
        CreateObjectFromArray(defender_kings,'defender_king',data)
        CreateObjectFromArray(attacker_outcomes,'attacker_outcome',data)

        const output = {
            'most_active':{
                'attacker_king':GetMaxCountKey(attacker_kings),
                'defender_king':GetMaxCountKey(defender_kings),
                'region':GetMaxCountKey(regions)
            },
            'attacker_outcome':{
               ...attacker_outcomes
            },
            'battle_type':Array.from(battle_types).filter(el=>el.length>1), // unique battle types
            'defender_size':{
                'average':defender_sizes.reduce((acc,curr)=>(acc+curr),0)/defender_sizes.length,
                'min':Math.min(...defender_sizes),
                'max':Math.max(...defender_sizes)
                }
            }
            
        console.log('output:',output)

    })
