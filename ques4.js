fetch('https://think.cs.vt.edu/corgis/datasets/json/airlines/airlines.json')
    .then(res=>res.json())
    .then(data=>{
      console.log(data)
        data.forEach(el => {
          // Airport code and stats
          console.log(el.Airport['Code'],el.Statistics['Flights'])

          const details = Array.from(Object.values(el.Statistics['Flights']))
          
          // return true if sum of all types of flights is equal to total
          console.log(details.slice(0,4).reduce((a,b)=>a+b,0) === details[details.length-1])
        });
    })