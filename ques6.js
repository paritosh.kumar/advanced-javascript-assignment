class QueenAttack {
    constructor(posOne, posTwo){
        this.posOne = posOne;
        this.posTwo = posTwo;
    }

    canAttack() {
        if (this.posOne[0] === this.posTwo[0] || this.posOne[1] === this.posTwo[1] || Math.abs(this.posOne[0]-this.posTwo[0]) === Math.abs(this.posTwo[1] - this.posOne[1]))
            return true
        else
            return false
    }
}


let value1 = prompt('enter value in x,y format')
let value2 = prompt('enter value in x,y format')

value1 = value1.split(',').map(el=>parseInt(el))
value2 = value2.split(',').map(el=>parseInt(el))
const test = new QueenAttack(value1,value2)

console.log(test.canAttack() ? 'Queens can attack' : 'Cannot attack each other')